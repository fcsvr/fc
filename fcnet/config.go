package fcnet

import (
	"io/ioutil"
	"os"
	"path"

	jsoniter "github.com/json-iterator/go"
)

type SvrConfig struct {
	Name          string
	IP            string
	IPVer         string
	TCPPort       int
	HomeDir       string
	LogDir        string
	MaxPackLen    uint
	MaxConn       uint
	WorkerPoolLen uint
	WorkerTaskLen uint
	MaxChanLen    uint
}

type ConfigMgr struct {
	Svr *SvrConfig
}

func NewConfigMgr() *ConfigMgr {
	c := &ConfigMgr{
		Svr: &SvrConfig{
			Name:          "FanSvr",
			IP:            "0.0.0.0",
			IPVer:         "tcp4",
			TCPPort:       7010,
			HomeDir:       "",
			MaxConn:       10,
			MaxPackLen:    4096,
			WorkerPoolLen: 10,
			WorkerTaskLen: 10,
			MaxChanLen:    10,
			LogDir:        "",
		},
	}
	c.Svr.HomeDir, _ = os.Getwd()
	c.Svr.LogDir = path.Join(c.Svr.HomeDir, "log")
	return c
}

func (this *ConfigMgr) Load() {
	data, err := ioutil.ReadFile("conf/config.json")
	if err != nil {
		return
	}
	err = jsoniter.Unmarshal(data, this.Svr)
	if err != nil {
		return
	}
}
