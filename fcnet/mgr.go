package fcnet

type Mgr struct {
	svr       *Server
	conn      *ConnMgr
	msgHandle *MsgHandleMgr
	config    *ConfigMgr
	sync      *SyncMgr
}

var SvrMgr *Mgr

func init() {
	SvrMgr = &Mgr{}
	SvrMgr.config = NewConfigMgr()
	SvrMgr.config.Load()
	SvrMgr.svr = NewServer()
	SvrMgr.conn = NewConnMgr()
	SvrMgr.msgHandle = NewMsgHandleMgr()
	SvrMgr.sync = NewSyncMgr()
}

func GetSvrMgr() *Server {
	return SvrMgr.svr
}

func (this *Mgr) Svr() *Server {
	return this.svr
}

func (this *Mgr) Conn() *ConnMgr {
	return this.conn
}

func (this *Mgr) MsgHandle() *MsgHandleMgr {
	return this.msgHandle
}

func (this *Mgr) Config() *ConfigMgr {
	return this.config
}

func (this *Mgr) ConfigSvr() *SvrConfig {
	return this.config.Svr
}

func (this *Mgr) Sync() *SyncMgr {
	return this.sync
}
