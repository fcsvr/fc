package fcnet

import "sync"

type SyncMgr struct {
	connWp *sync.WaitGroup
}

func NewSyncMgr() *SyncMgr {
	s := &SyncMgr{
		connWp: new(sync.WaitGroup),
	}
	return s
}

func (this *SyncMgr) ConnWp() *sync.WaitGroup {
	return this.connWp
}
