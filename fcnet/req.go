package fcnet

type Req struct {
	*Conn
	*Msg
}

func NewReq(conn *Conn, msg *Msg) *Req {
	r := &Req{
		Conn: conn,
		Msg:  msg,
	}
	return r
}

func (this *Req) GetConn() *Conn {
	return this.Conn
}

func (this *Req) GetMsg() *Msg {
	return this.Msg
}
