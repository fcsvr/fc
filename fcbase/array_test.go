package fcbase

//go test -v **_test.go
//go test -v **_test.go -test.run Test**

import (
	"fmt"
	"testing"
)

func TestArray(t *testing.T) {
	a := [3]int{1, 2, 3}
	b := a
	if a == b {
		fmt.Println(true)
	}
	fmt.Println(cap(a))
	fmt.Println(len(a))
	c := 2
	fmt.Println(a[c])
}
