package fcbase

//go test -v **_test.go
//go test -v **_test.go -test.run Test**

import (
	"fmt"
	"testing"
)

func TestSlice(t *testing.T) {
	a := make([]int, 5, 10)
	a = append(a, 1)
	fmt.Println(a[0], a[5])
}
