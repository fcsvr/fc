package fcbase

//使用once来实现单例模式

import (
	"fmt"
	"sync"
	"testing"
	"time"
	"unsafe"
)

type Singleton struct {
}

var singleInstance *Singleton
var once sync.Once

func GetSingleObj() *Singleton {
	once.Do(func() {
		fmt.Println("create single")
		singleInstance = new(Singleton)
	})
	return singleInstance
}

func TestOnce(t *testing.T) {
	for i := 0; i < 10; i++ {
		go func() {
			obj := GetSingleObj()
			fmt.Println(unsafe.Pointer(obj))
		}()
	}
	time.Sleep(time.Second)
}
