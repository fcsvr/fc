package fcbase

import (
	"encoding/json"
	"strconv"
	"testing"
)

type Request struct {
	TransactionID string
	Payload       []int
}
type Response struct {
	TransactionID string
	Ret           string
}

func createReq() string {
	payload := make([]int, 100, 100)
	for i := 0; i < 100; i++ {
		payload[i] = i
	}

	req := Request{"demo_transcation", payload}
	v, err := json.Marshal(&req)
	if err != nil {
		panic(err)
	}
	return string(v)
}

func processReq(reqs []string) []string {
	reps := []string{}
	for _, req := range reqs {
		reqobj := &Request{}
		json.Unmarshal([]byte(req), reqobj)
		ret := ""
		for _, e := range reqobj.Payload {
			ret += strconv.Itoa(e) + ","
		}
		repobj := &Response{reqobj.TransactionID, ret}
		repJosn, err := json.Marshal(&repobj)
		if err != nil {
			panic(err)
		}
		reps = append(reps, string(repJosn))
	}
	return reps
}

func TestCreateReq(t *testing.T) {
	str := createReq()
	t.Log(str)
}

func TestProcessReq(t *testing.T) {
	reqs := []string{}
	reqs = append(reqs, createReq())
	reps := processReq(reqs)
	t.Log(reps[0])
}

func BenchmarkProcessReq(b *testing.B) {
	reqs := []string{}
	reqs = append(reqs, createReq())
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = processReq(reqs)
	}
	b.StopTimer()
}
