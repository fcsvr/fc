package fcbase

//go test -v chan_test.go
//go test -v **_test.go -test.run Test**

import (
	"fmt"
	"testing"
)

func TestChan(t *testing.T) {
	a := make(chan int, 1)
	fmt.Println("cap chan = ", cap(a))
	c := <-a
	fmt.Println(c)
	b := a
	if a == b {
		fmt.Println(true)
	} else {
		fmt.Println(false)
	}
}
