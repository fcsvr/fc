package fcbase

//go test -v interface_test.go
//go test -v interface_test.go -bench=. -benchmem
//go test -v **_test.go -test.run Test**

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
)

type Sumifier interface {
	Add(a, b int32) int32
}

type Sumer struct {
	id   int32
	name string
}

//go:noinline
func (this *Sumer) Add(a, b int32) int32 {
	return a + b
}

func BenchmarkDirect(b *testing.B) {
	adder := Sumer{
		id: 6754,
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		adder.Add(10, 32)
	}
}

func BenchmarkInterface(b *testing.B) {
	adder := Sumifier(&Sumer{id: 6753})
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		adder.Add(10, 32)
	}
}

func BenchmarkInterfacePointer(b *testing.B) {
	adder := Sumifier(&Sumer{id: 6753})
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		adder.Add(10, 32)
	}
}

type User struct {
	ID   int
	Name string
	Lv   int
}

func AddUser(u any) {
	a := u.(*User)
	fmt.Println("AddUser = ", a.ID, a.Name, a.Lv)
}

func TestUser(t *testing.T) {
	u1 := User{
		ID:   1,
		Name: "fancy",
		Lv:   2,
	}
	fmt.Println("u1 = ", u1)
	uData, _ := json.Marshal(u1)
	fmt.Println("uData = ", uData)
	fmt.Println("uData = ", string(uData))

	userTyp := reflect.TypeOf(u1)
	userV := reflect.New(userTyp).Elem()
	userI := userV.Addr().Interface()

	json.Unmarshal(uData, &userI)
	fmt.Println("userI = ", userI)

	fmt.Println(reflect.TypeOf(userI))

	AddUser(userI)

	var buff bytes.Buffer
	enc := gob.NewEncoder(&buff)
	enc.Encode(u1)
	fmt.Println(buff.String())

	dec := gob.NewDecoder(&buff)
	dec.Decode(&userI)
	fmt.Println(userI)
}
