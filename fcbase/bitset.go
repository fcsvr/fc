package fcbase

//0.1
//实现用bit位来当作set用, 这样一个64的word值就能存64个值, 有效利用内存
//但从算法上看也有弊端, 比如要存的两个值相差比较大, 比如是1和10000000
//那要开辟的内存也比较多, 而用原始的map来存, 只要两个int值就好了
//具体就不在实现了, 用到的时候参考go语言圣经6.5节
//0.2
//想想可以用一个额外的map来存储索引, 用v/64来做key, 这样用到的时候直接创建, 没有了就销毁

/*
type BitSet struct {
	words []uint64
}

func (this *BitSet) Has(v int) bool {
	word, bit := v/64, uint(v%64)
	return word < len(this.words) && this.words[word]&(1<<bit) != 0
}

func (this *BitSet) Add(v int) {
	word, bit := v/64, uint(v%64)
	for word >= len(this.words) {
		this.words = append(this.words, 0)
	}
	this.words |= 1 << bit
}
*/
