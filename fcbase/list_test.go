package fcbase

import (
	"testing"
)

func TestList(t *testing.T) {
	lhead := NewLNode()
	CreateList(lhead, []int{1, 2, 3, 4, 5})
	PrintList(lhead)
}

func BenchmarkListReverse(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		lhead := NewLNode()
		CreateList(lhead, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6})
		Reverse(lhead)
	}
	b.StopTimer()
}
func BenchmarkListReverseInsert(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		lhead := NewLNode()
		CreateList(lhead, []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6})
		ReverseInsert(lhead)
	}
	b.StopTimer()
}
