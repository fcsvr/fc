package fcbase

import (
	"fmt"
	"time"
)

func TimeFmt() {
	s := time.Now().Format("15:04:05\n")
	fmt.Println(s)
}

func init() {
	go func() {
		ticker := time.NewTicker(time.Second)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				fmt.Println(1)
			}
		}
	}()
}
