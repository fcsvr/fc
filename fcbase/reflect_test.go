package fcbase

//go test -v reflect_test.go
//go test -v **_test.go -test.run Test**

import (
	"fmt"
	"reflect"
	"testing"
	"unsafe"
)

type Base struct {
	a int
}

type HelloArg struct {
	Base
	ID    int
	MsgID int64
	Name  string
}

type HelloReply struct {
	ID  int
	Err error
}

type BaseHandle struct {
	Argvi any
}

func (this *BaseHandle) Hello() {
	fmt.Println("base")
}

func (this *BaseHandle) SetArgvi(argvi any) {
	this.Argvi = argvi
}
func (this *BaseHandle) GetArgvi() any {
	return this.Argvi
}

type PingHandle struct {
	BaseHandle
	a int
}

func (this *PingHandle) CurHandle(req int) {
	fmt.Println("PingHandle req = ", req)
	fmt.Println("PingHandle req = ", this.a)
}

func (this *PingHandle) Hello() {
	fmt.Println("PingHandle")
}

func Hello(handle *BaseHandle) {
	h := (*PingHandle)(unsafe.Pointer(handle))
	h.Hello()
}

func TestFuncReflect(t *testing.T) {
	a := reflect.TypeOf(Hello)
	fmt.Println(a)
}

type HelloService struct {
}

func (p *HelloService) Hello(arg HelloArg, reply *HelloReply) error {
	reply.ID = arg.ID
	return nil
}

func TestStrutReflect(t *testing.T) {
	var a HelloArg = HelloArg{
		Base: Base{
			a: int(1),
		},
		ID:    1,
		MsgID: 1,
		Name:  "fancy",
	}
	var r HelloReply
	var s HelloService

	aType := reflect.TypeOf(a)
	aValue := reflect.ValueOf(a)
	//fcbase.HelloArg
	fmt.Println(aType)
	//HelloArg
	fmt.Println(aType.Name())
	//struct
	fmt.Println(aType.Kind())
	//{1 1 fancy}
	fmt.Println(aValue)
	//结构体各字段
	for i := 0; i < aType.NumField(); i++ {
		fmt.Println("hello arg field = ", aType.Field(i))
		fmt.Println("hello arg field = ", aValue.Field(i).Interface())
	}
	//方法名
	sType := reflect.TypeOf(&s)
	fmt.Println("stype method num = ", sType.NumMethod(), sType.Name())
	for i := 0; i < sType.NumMethod(); i++ {
		mType := sType.Method(i).Type
		fmt.Println(sType.Method(i).Name)
		fmt.Println(mType)
		fmt.Println(mType.NumIn(), mType.NumOut())
	}

	fmt.Println("////////")
	fmt.Println(reflect.TypeOf(r))
	fmt.Println(reflect.ValueOf(r))

	fmt.Println(reflect.TypeOf(s))
	fmt.Println(reflect.ValueOf(s))
}

func TestReflect(t *testing.T) {
	a := 1
	d := &a
	b := reflect.ValueOf(a).Interface().(int)
	c := reflect.ValueOf(a).Int()
	e := reflect.ValueOf(d).Interface().(*int)
	h := reflect.ValueOf(d).Elem().Int()
	fmt.Println(a, b, c, *e, h)
}
