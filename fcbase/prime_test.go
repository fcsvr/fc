package fcbase

import (
	"fmt"
	"testing"
)

func GenerateNatural() chan int {
	ch := make(chan int)
	go func() {
		for i := 3; ; i += 2 {
			ch <- i
		}
	}()
	return ch
}

var Cm chan int

func PrimeFilter(in <-chan int, prime int) chan int {
	out := make(chan int)
	Cm = out //每次调用该函数初始化筛子的时候该筛子必为最大一个，
	go func() {
		for {
			if i := <-in; i%prime != 0 {
				if i < prime*prime { //满足临界条件，直接将值传递给最后一个素数筛
					Cm <- i
				} else {
					out <- i
				}
			}
		}
	}()
	return out
}

func TestPrime(t *testing.T) {
	ch := GenerateNatural() // 自然数序列: 2, 3, 4, ...
	fmt.Println("0:2")
	for i := 1; i < 10; i++ {
		prime := <-ch
		fmt.Printf("%d:%d \n", i, prime) // 新出现的素数
		ch = PrimeFilter(ch, prime)      // 基于新素数构造的过滤器
	}
}
