package fcbase

//go test -v type_test.go
//go test -v **_test.go -test.run Test**

import (
	"fmt"
	"testing"
	"unsafe"
)

const (
	LEN  int8  = 9
	DATA int16 = 10
)

type Head struct {
	ID        uint32
	BodyLen   uint32
	Magic     uint16
	HeadExLen uint16
	Typ       uint8
	Version   uint8
	CodecTyp  uint8
	HeadLen   uint8
}

func TestType(t *testing.T) {
	var a int = 1
	var b int8 = 2
	var c int16 = 3
	var d int32 = 4
	var e int64 = 5
	fmt.Println(unsafe.Sizeof(a))
	fmt.Println(unsafe.Sizeof(b))
	fmt.Println(unsafe.Sizeof(c))
	fmt.Println(unsafe.Sizeof(d))
	fmt.Println(unsafe.Sizeof(e))
	fmt.Println(LEN)
	fmt.Println(DATA)
	fmt.Printf("sizeof = %d\n", unsafe.Sizeof(Head{}))
}
