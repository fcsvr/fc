package fcbase

//go test -v **_test.go
//go test -v **_test.go -test.run Test**

import (
	"fmt"
	"testing"
)

var g1 = 100

func f1() (r int) {
	r = g1
	defer func() {
		r = 200
	}()
	r = 0
	return r
}
func TestDefer1(t *testing.T) {
	i := f1()
	fmt.Printf("main: i = %d, g = %d\n", i, g1)
}

var g2 = 100

func f2() (r int) {
	defer func() {
		g2 = 200
		fmt.Printf("f2: r = %d\n", r)
	}()
	return g2
}
func TestDefer2(t *testing.T) {
	i := f2()
	fmt.Printf("main: i = %d, g = %d\n", i, g2)
}
