package fcbase

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func hello(ctx context.Context) bool {
	for {
		select {
		case <-ctx.Done():
			fmt.Println("cancel")
			return true
		default:
			time.Sleep(time.Millisecond)
		}
	}
}

func TestCancel(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	go hello(ctx)
	time.Sleep(time.Second)
	cancel()
	time.Sleep(time.Second)
}
