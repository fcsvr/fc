package fcbase

import (
	"fmt"
	"runtime"
	"testing"
	"time"
)

func runTask(id int) string {
	time.Sleep(10 * time.Millisecond)
	return fmt.Sprintf("The result is from %d", id)
}

func FirstRsp() string {
	num := 10
	ch := make(chan string)
	for i := 0; i < num; i++ {
		go func(i int) {
			ret := runTask(i)
			ch <- ret
		}(i)
	}
	return <-ch
}

func TestFirstrsp(t *testing.T) {
	t.Log("before:", runtime.NumGoroutine())
	t.Log(FirstRsp())
	time.Sleep(time.Second)
	t.Log("after:", runtime.NumGoroutine())
}
