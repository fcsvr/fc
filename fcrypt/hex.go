package fcrypt

import "encoding/hex"

func Hex(data []byte) string {
	res := hex.EncodeToString(data)
	return res
}

func HexStr(str string) string {
	data := []byte(str)
	return Hex(data)
}

func HexByte(data []byte) []byte {
	dst := make([]byte, 2*len(data), 2*len(data))
	hex.Encode(dst, data)
	return dst
}
