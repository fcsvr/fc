package fcrypt

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
)

func RSAEncrype(plainByte []byte, file string) ([]byte, error) {
	publicKey, err := ReadPublicKey(file)
	if err != nil {
		fmt.Println("read public key err = ", err)
		return nil, err
	}

	return rsa.EncryptPKCS1v15(rand.Reader, publicKey, plainByte)
}

func RSADecrypt(cipherByte []byte, file string) ([]byte, error) {
	privateKey, err := ReadPrivateKey(file)
	if err != nil {
		fmt.Println("read private key err = ", err)
		return nil, err
	}
	return rsa.DecryptPKCS1v15(rand.Reader, privateKey, cipherByte)
}
