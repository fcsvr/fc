package fcrypt

import (
	"net/url"
)

func UrlEncode(str string) string {
	return url.QueryEscape(str)
}

func UrlDecode(str string) string {
	res, _ := url.QueryUnescape(str)
	return res
}
