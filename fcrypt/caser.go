package fcrypt

import (
	"fmt"
	"strings"
)

//移位密码，也称作凯撒密码，因为是凯撒当年打仗的时候开始使用的
func CaesarEnc(plain string, step byte) string {
	plainLower := strings.ToLower(plain)
	plainByte := []byte(plainLower)
	plainLen := len(plainByte)
	fmt.Printf("明文字节切片 = %v\n", plainByte)

	cipherByte := make([]byte, plainLen, plainLen)

	for i := 0; i < plainLen; i++ {
		if plainByte[i]+step <= 122 {
			cipherByte[i] = plainByte[i] + step
		} else {
			cipherByte[i] = plainByte[i] + step - 26
		}
	}
	fmt.Printf("步长 = %d, 加密后字节 = %v\n", step, cipherByte)
	return string(cipherByte)
}

func CaesarDec(cipher string, step byte) string {
	cipherByte := []byte(cipher)
	fmt.Printf("密文字节切片 = %v\n", cipherByte)

	plainByte := cipherByte

	for i := 0; i < len(cipherByte); i++ {
		if cipherByte[i]-step >= 97 {
			plainByte[i] = cipherByte[i] - step
		} else {
			plainByte[i] = cipherByte[i] - step + 26
		}
	}
	fmt.Printf("步长 = %d, 解密密后字节 = %v\n", step, plainByte)
	return string(plainByte)
}
