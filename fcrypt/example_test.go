package fcrypt

import (
	"crypto"
	"crypto/sha256"
	"encoding/hex"
	"fmt"

	"gitee.com/fcsvr/fc/fcrypt"
)

func ExampleHello() {
	fmt.Printf("%s\n\n", "--------------------密码学编程---------------------")
	//FC_Math()
	//FC_Hex()
	//FC_UrlEncode()
	//FC_MorseEncode()
	//FC_BaseEncode()
	//FC_Caser()
	//FC_Curve()
	//FC_Yunying()
	//FC_Rail()
	FC_Affine()
	//FC_Hash()
	//FC_Rsa()
	//FC_Aes()
	//FC_Sign()
	//FC_Ecdsa()

	//OutPut:
}

func FC_Math() {
	fmt.Printf("%s\n", "--------------------Math方法开始-----------------------")
	a := 851
	b := 26
	c := fcrypt.Gcd(a, b)
	fmt.Printf("%d 和 %d 的最大公因数 = %d\n", a, b, c)
	i := fcrypt.GetModInverse(7, 26)
	fmt.Printf("模逆= %d\n", i)
	fmt.Printf("%s\n\n", "--------------------Math方法结束-----------------------")
}

func FC_Hex() {
	fmt.Printf("%s\n", "--------------------Hex开始-----------------------")
	str := "g"
	strByte := []byte(str)
	strHexByte := fcrypt.HexByte(strByte)
	strHex := fcrypt.Hex(strByte)
	fmt.Printf("原字节数组 = %v, 原字符串 = %s, Hex编码后的字符串 = %s, byte值 = %v\n", strByte, str, strHex, strHexByte)

	b := []byte{17, 226}
	bStr := string(b)
	bHexByte := fcrypt.HexByte(b)
	bHex := fcrypt.Hex(b)
	fmt.Printf("原字节数组 = %v, 原字符串 = %s, Hex编码后的字符串 = %s, byte值 = %v\n", b, bStr, bHex, bHexByte)
	fmt.Printf("%s\n\n", "--------------------Hex结束-----------------------")
}

func FC_UrlEncode() {
	fmt.Printf("%s\n", "--------------------UrlEncode开始-----------------------")
	str := "Hello, crypt. 你好密码学！ 我"
	urlStr := fcrypt.UrlEncode(str)
	fmt.Println("Url编码 =", urlStr)
	fmt.Println("Url编码 =", []byte(urlStr))

	str = fcrypt.UrlDecode(urlStr)
	fmt.Println("Url解码 =", str)
	fmt.Printf("%s\n\n", "--------------------UrlEncode结束-----------------------")
}

func FC_MorseEncode() {
	fmt.Printf("%s\n", "--------------------MorseEncode开始-----------------------")
	str := "Hello crypt."
	res := fcrypt.StrToMorse(str)
	fmt.Println("摩斯编码 =", res)
	fmt.Printf("%s\n\n", "--------------------MorseEncode结束-----------------------")
}

func FC_BaseEncode() {
	fmt.Printf("%s\n", "--------------------Base开始-----------------------")
	str := "Hello crypt."
	fmt.Printf("原字符串 = %s\n", str)
	res32 := fcrypt.StrToBase32(str)
	fmt.Println("Base32编码后字符串 =", res32)
	res32, _ = fcrypt.Base32ToStr(res32)
	fmt.Println("Base32解码后字符串 =", res32)
	res64 := fcrypt.StrToBase64(str)
	fmt.Println("Base64编码后字符串 =", res64)
	res64, _ = fcrypt.Base64ToStr(res64)
	fmt.Println("Base64解码后字符串 =", res64)
	fmt.Printf("%s\n\n", "--------------------Base结束-----------------------")
}

func FC_Caser() {
	fmt.Printf("%s\n", "--------------------移位密码开始-----------------------")
	plain := "Hello Vimgo"
	fmt.Printf("明文 = %s\n", plain)
	cipher := fcrypt.CaesarEnc(plain, 1)
	fmt.Printf("密文 = %s\n", cipher)
	plain = fcrypt.CaesarDec(cipher, 1)
	fmt.Printf("明文 = %s\n", plain)
	fmt.Printf("%s\n\n", "--------------------移位密码结束-----------------------")
}

func FC_Curve() {
	fmt.Printf("%s\n", "--------------------曲路密码开始-----------------------")
	plain := "Hello vimgo"
	fmt.Printf("明文 = %s\n", plain)
	cipher := fcrypt.CurveEnc(plain, 4)
	fmt.Printf("密文 = %s\n", cipher)
	plain = fcrypt.CurveDec(cipher, 4)
	fmt.Printf("解密后明文 = %s\n", plain)
	fmt.Printf("%s\n\n", "--------------------曲路密码结束-----------------------")
}

func FC_Yunying() {
	fmt.Printf("%s\n", "--------------------云影密码开始-----------------------")
	plain := "Hellovimgo"
	fmt.Printf("明文 = %s\n", plain)
	cipher := fcrypt.YunyingEnc(plain)
	fmt.Printf("密文 = %s\n", cipher)
	plain = fcrypt.YunyingDec(cipher)
	fmt.Printf("解密后明文 = %s\n", plain)
	fmt.Printf("%s\n\n", "--------------------云影密码结束-----------------------")

}

func FC_Rail() {
	fmt.Printf("%s\n", "--------------------栅栏密码开始-----------------------")
	plain := "Hellovimgo"
	fmt.Printf("明文 = %s\n", plain)
	cipher := fcrypt.RailEnc(plain, 2)
	fmt.Printf("密文 = %s\n", cipher)
	plain = fcrypt.RailDec(cipher, 2)
	fmt.Printf("解密后明文 = %s\n", plain)
	fmt.Printf("%s\n\n", "--------------------栅栏密码结束-----------------------")
}

func FC_Affine() {
	fmt.Printf("%s\n", "--------------------仿射密码开始-----------------------")
	key := fcrypt.AffineGetKey()
	plain := "Hellovimgo"
	fmt.Printf("明文 = %s\n", plain)
	cipher := fcrypt.AffineEnc(plain, key)
	fmt.Printf("密文 = %s\n", cipher)
	plain = fcrypt.AffineDec(cipher, key)
	fmt.Printf("解密后明文 = %s\n", plain)
	fmt.Printf("%s\n\n", "--------------------仿射密码结束-----------------------")
}

func FC_Hash() {
	fmt.Printf("%s\n", "--------------------哈希开始-----------------------")
	str := "hello"
	fmt.Println(fcrypt.HashStr(crypto.MD5, str))
	fmt.Println(fcrypt.HashStr(crypto.SHA512, str))
	fmt.Printf("%s\n\n", "--------------------哈希结束-----------------------")
}

func FC_Rsa() {
	fmt.Printf("%s\n", "--------------------私钥加密开始-----------------------")
	plainStr := "vimgo"
	fmt.Printf("明文字符串 = %s\n", plainStr)
	plainByte := []byte(plainStr)
	fmt.Printf("明文字节流 = %x\n", plainByte)
	cipherByte, _ := fcrypt.RSAEncrype(plainByte, "./public.key")
	fmt.Printf("加密后字节流 = %x\n", cipherByte)
	cipherHex := fmt.Sprintf("%x", cipherByte)
	fmt.Printf("加密后hex字节流 = %x\n", cipherHex)

	plainByte, _ = fcrypt.RSADecrypt(cipherByte, "./private.key")
	plainStr = string(plainByte)
	fmt.Printf("解密后字符串 = %s\n", plainStr)

	fmt.Printf("%s\n\n", "--------------------私钥加密结束-----------------------")
}

func FC_Sign() {
	fmt.Printf("%s\n", "--------------------数字签名开始-----------------------")
	plainStr := "vimgo"
	signStr, err := fcrypt.RSASign([]byte(plainStr), "./private.key")
	if err != nil {
		return
	}

	fmt.Printf("签名后的字符串 = %s\n", signStr)

	err = fcrypt.RSAVerify([]byte(plainStr), signStr, "./public.key")
	if err == nil {
		fmt.Println("签名验证成功")
	} else {
		fmt.Println("签名验证失败")
	}
	fmt.Printf("%s\n\n", "--------------------数字签名结束-----------------------")
}

func FC_Aes() {
	fmt.Printf("%s\n", "--------------------对称加密开始---------------------")
	fmt.Println("--------------------设置参数-----------------------")
	plainStr := "hello"
	plainByte := []byte(plainStr)

	//8个字节(64位)
	desKey := []byte("12345678")
	//24个字节(192位)
	tripdesKey := []byte("123456789012345678901234")
	//16个字节(128位)-aes128
	//24个字节(192位)-aes192
	//32个字节(256位)-aes256
	aesKey := []byte("12345678901234567890123456789012")

	fmt.Println("明文字符串 = ", plainStr)
	fmt.Println("明文字节流 = ", plainByte)

	fmt.Println("des密钥 = ", string(desKey))
	fmt.Println("3des密钥 = ", string(tripdesKey))
	fmt.Println("aes密钥 = ", string(aesKey))

	fmt.Println("--------------------加密-----------------------")
	desCipherByte, _ := fcrypt.SCEncrypt(plainByte, desKey, "des")
	fmt.Printf("des加密后字节流 = %v\n", desCipherByte)
	//a := fmt.Sprintf("%x", desCipherByte)
	desCipherHex := hex.EncodeToString(desCipherByte)
	fmt.Println("des加密后hex字节流 = ", desCipherHex)

	tripdesCipherByte, _ := fcrypt.SCEncrypt(plainByte, tripdesKey, "3des")
	fmt.Println("3des加密后字节流 = ", tripdesCipherByte)
	tripdesCipherHex := hex.EncodeToString(tripdesCipherByte)
	fmt.Println("3des加密后hex字节流 = ", tripdesCipherHex)

	aesCipherByte, _ := fcrypt.SCEncrypt(plainByte, aesKey, "aes")
	fmt.Println("aes加密后字节流 = ", aesCipherByte)
	aesCipherHex := hex.EncodeToString(aesCipherByte)
	fmt.Println("aes加密后hex字节流 = ", aesCipherHex)

	fmt.Println("--------------------解密-----------------------")
	aesPlainByte, _ := fcrypt.SCDecrypt(aesCipherByte, aesKey, "aes")
	fmt.Println("aes解密后的字符串 = ", string(aesPlainByte))
	desPlainByte, _ := fcrypt.SCDecrypt(desCipherByte, desKey, "des")
	fmt.Println("des解密后的字符串 = ", string(desPlainByte))
	tripdesPlainByte, _ := fcrypt.SCDecrypt(tripdesCipherByte, tripdesKey, "3des")
	fmt.Println("3des解密后的字符串 = ", string(tripdesPlainByte))

	fmt.Printf("%s\n\n", "--------------------私钥加密结束-----------------------")
}

func FC_Ecdsa() {
	fmt.Printf("%s\n", "--------------------椭圆曲线验证开始---------------------")
	data := "vimgo"
	hashIns := sha256.New()
	hashIns.Write([]byte(data))
	hashByte := hashIns.Sum(nil)

	priKey, pubKey := fcrypt.NewKey()

	derStr := fcrypt.ECDSASign(hashByte, priKey)
	fmt.Println("签名信息 = ", derStr)

	fmt.Printf("NewKey pubKeyByte len = %d\n", len(pubKey))
	flag := fcrypt.ECDSAVerify(hashByte, pubKey, derStr)
	fmt.Println("验证结果", flag)
	fmt.Printf("%s\n\n", "--------------------椭圆曲线验证结束---------------------")
}
