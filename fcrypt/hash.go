package fcrypt

import (
	"crypto"
	"fmt"
	"io"
)

const (
	Hash_MD5    = "md5"
	Hash_SHA512 = "sha512"
)

func HashStr(hashTyp crypto.Hash, str ...string) string {
	hash := hashTyp.New()
	for _, v := range str {
		io.WriteString(hash, v)
	}
	out := hash.Sum(nil)
	outString := fmt.Sprintf("%x", out)
	return outString
}

func HashByte(hashTyp crypto.Hash, str ...string) []byte {
	hash := hashTyp.New()
	for _, v := range str {
		io.WriteString(hash, v)
	}
	out := hash.Sum(nil)
	return out
}
