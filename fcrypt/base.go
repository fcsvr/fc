package fcrypt

import (
	"encoding/base32"
	"encoding/base64"
)

func StrToBase32(str string) string {
	b := []byte(str)
	res := base32.StdEncoding.EncodeToString(b)
	return res
}

func Base32ToStr(str string) (string, error) {
	res, err := base32.StdEncoding.DecodeString(str)
	return string(res), err
}

func StrToBase64(str string) string {
	b := []byte(str)
	res := base64.StdEncoding.EncodeToString(b)
	return res
}

func Base64ToStr(str string) (string, error) {
	res, err := base64.StdEncoding.DecodeString(str)
	return string(res), err
}
