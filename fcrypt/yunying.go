package fcrypt

import "strings"

//云影密码，01248码，先将26个字母对应到数字1-26，然后将数字表示成01248的和的格式
func YunyingEnc(plain string) string {
	plain = strings.ToLower(plain)
	plainByte := []byte(plain)
	cipher := ""
	for _, v := range plainByte {
		val := v - 0x60
		for {
			if val >= 8 {
				cipher += "8"
				val -= 8
			} else if val >= 4 {
				cipher += "4"
				val -= 4
			} else if val >= 2 {
				cipher += "2"
				val -= 2
			} else if val >= 1 {
				cipher += "1"
				val -= 1
			} else {
				cipher += "0"
				break
			}
		}
	}

	return cipher
}

func YunyingDec(cipher string) string {
	cipherArr := strings.Split(cipher, "0")
	plainByte := make([]byte, 0, 0)
	for _, v := range cipherArr {
		val := 0
		for _, valUnit := range v {
			val += (int(valUnit) - 0x30)
		}
		plainByte = append(plainByte, byte(val+0x60))
	}
	return string(plainByte)
}
