package fconfig

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"os"
)

type Website struct {
	Name   string `xml:"name,attr"`
	Url    string
	Course []string
}

func ExampleHello() {
	fmt.Printf("%s\n\n", "--------------------配置文件---------------------")
	FC_Xml()
	//FC_FileRead()
	FC_FileWrite()

	//OutPut:
}

func FC_Xml() {
	info := Website{
		"C语言中文网",
		"http://c.biancheng.net/golang/",
		[]string{
			"Go语言入门教程",
			"Golang入门教程",
		},
	}
	f, err := os.Create("./info.xml")
	if err != nil {
		fmt.Println("文件创建失败", err.Error())
		return
	}
	defer f.Close()
	//序列化到文件中
	encoder := xml.NewEncoder(f)
	err = encoder.Encode(info)
	if err != nil {
		fmt.Println("编码错误：", err.Error())
		return
	} else {
		fmt.Println("编码成功")
	}
}

func FC_FileRead() {
	var filePath string = `./stock.info`
	FileHandle, err := os.Open(filePath)
	if err != nil {
		log.Println(err)
		return
	}
	defer FileHandle.Close()
	lineReader := bufio.NewReader(FileHandle)
	for {
		line, _, err := lineReader.ReadLine()
		if err == io.EOF {
			break
		}
		fmt.Println(string(line))
	}
}

func FC_FileWrite() {
	var filePath string = `./stock.info`
	FileHandle, err := os.OpenFile(filePath, os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		log.Println(err)
		return
	}

	var str string
	for i := 0; i <= 10; i++ {
		str += "sh000001,"
		str += "aaaa,"
		str += "56"
		str += "\n"
	}
	b := []byte(str)
	_, err = FileHandle.Write(b)
	if err != nil {
		fmt.Println(err)
	}
}
