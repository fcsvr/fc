module gitee.com/fcsvr/fc

go 1.19

replace gitee.com/fcsvr/fc => /root/fc

require (
	github.com/json-iterator/go v1.1.12
	github.com/vmihailenco/msgpack/v5 v5.3.5
)

require (
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
)
