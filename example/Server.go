package main

import (
	"gitee.com/fcsvr/fc/fclog"
	"gitee.com/fcsvr/fc/fcnet"
)

type PingHandle struct {
	fcnet.BaseHandle
}

type PingArg struct {
	str string
}

func (this *PingHandle) CurHandle(req *fcnet.Req) {
	fclog.Debug("PingRouter Handle")
	fclog.Debug("Recv from cli: msgid = %d, data = %s", req.GetMsgID(), string(req.GetMsgBody()))
	err := req.GetConn().SendMsg(uint32(1), []byte("Ping"))
	if err != nil {
		fclog.Error("PingRouter Handle: send msg err = %s", err)
	}
}

func main() {
	fclog.Info("Hello, FancyGo, This is FCSVR")
	server := fcnet.NewServer()

	//server.AddRouter(1, &PingRouter{})
	//server.AddRouter(2, &HelloRouter{})
	server.AddMsgHandle(1, &PingHandle{}, &PingArg{})
	server.Serve()
	/*
		for {
			time.Sleep(5 * time.Second)
		}
	*/
}
