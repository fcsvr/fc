package main

import (
	"encoding/json"
	"fmt"
	"net"
	"os"
	"os/user"
	"time"

	"gitee.com/fcsvr/fc/fcnet"
)

type PCArg struct {
	ID   string
	MAC  string
	User string
}

func CliTest() {
	fmt.Println("Client Test, Start")

	conn, err := net.Dial("tcp", "127.0.0.1:7010")
	if err != nil {
		fmt.Println("client start err: ", err)
		return
	}

	hostname, _ := os.Hostname()
	currentUser, _ := user.Current()

	pc := PCArg{
		ID:   hostname,
		MAC:  "0",
		User: currentUser.Username,
	}

	userByte, _ := json.Marshal(pc)
	fmt.Println(string(userByte))

	for {
		fmt.Println(string(userByte))
		msg := fcnet.NewMsg(1, userByte)
		buff, _ := msg.DoPack()
		fmt.Println("write msg = ", buff)
		_, err := conn.Write(buff)
		if err != nil {
			fmt.Println("write err: ", err)
			return
		}

		time.Sleep(5 * time.Second)
	}
}

func main() {
	go CliTest()
	for {
		time.Sleep(10 * time.Second)
	}
}
