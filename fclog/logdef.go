package fclog

const (
	LOG_MAX_FILE_IDX = 100
	LOG_DEF_MAX_SIZE = 100 * 1024 * 1024

	LOG_INTERVAL_DAY  = 1
	LOG_INTERVAL_HOUR = 2
	LOG_INTERVAL_MIN  = 3

	LOG_OUTPUT_STD  = 0x1 << 0
	LOG_OUTPUT_FILE = 0x1 << 1
	LOG_OUTPUT_SF   = LOG_OUTPUT_STD | LOG_OUTPUT_FILE
)

type Level int
type Color int

const (
	LV_TRACE Level = iota
	LV_DEBUG
	LV_INFO
	LV_TEST
	LV_WARN
	LV_ERROR
	LV_FATAL
)

const (
	NoColor Color = iota
	Black
	Red
	Green
	Yellow
	Blue
	Purple
	DarkGreen
	White
)

var logColorPrefix = map[Color]string{
	NoColor:   "",
	Black:     "\x1b[030m",
	Red:       "\x1b[031m",
	Green:     "\x1b[032m",
	Yellow:    "\x1b[033m",
	Blue:      "\x1b[034m",
	Purple:    "\x1b[035m",
	DarkGreen: "\x1b[036m",
	White:     "\x1b[037m",
}

var logColorSuffix = "\x1b[0m"

var lvStr = map[Level]string{
	LV_TRACE: "[TRACE]",
	LV_DEBUG: "[DEBUG]",
	LV_INFO:  "[INFO]",
	LV_TEST:  "[TEST]",
	LV_WARN:  "[WARN]",
	LV_ERROR: "[ERROR]",
	LV_FATAL: "[FATAL]",
}

var lvColor = map[Level]Color{
	LV_TRACE: NoColor,
	LV_DEBUG: Green,
	LV_INFO:  Blue,
	LV_TEST:  Red,
	LV_WARN:  Yellow,
	LV_ERROR: Purple,
	LV_FATAL: Red,
}

func getColorPrefixByLv(lv Level) string {
	c := lvColor[lv]
	s := logColorPrefix[c]
	return s
}

func getFlagStrByLv(lv Level) string {
	return lvStr[lv]
}
